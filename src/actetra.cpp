////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <SFML/Graphics.hpp>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iostream>

////////////////////////////////////////////////////////////
/// Entry point of application
///
/// \return Application exit code
///
////////////////////////////////////////////////////////////

int main()
{
    // Screen resolution
    const int pixWidth = 320, pixHeight = 240;
    
    // init empty grid with classic t*tris game grid dimensions 
    const int gridWidth = 10, gridHeight = 20;
    int gameGrid[gridHeight][gridWidth+1] = {0};

    // 10x10 pixel blocks
    sf::Vector2f blockSize(10, 10);

    // Create the window of the application
    sf::RenderWindow window(sf::VideoMode(pixWidth, pixHeight, 32), "actetra");
    window.setVerticalSyncEnabled(true);

    // set the form of our hero block
    sf::RectangleShape fallBlock;
    fallBlock.setSize(blockSize);
    fallBlock.setFillColor(sf::Color::White);
    const float blockSpeed = 1;

    // use the same dimensions for the fixed blocks, those part of the landscape
    sf::RectangleShape fixedBlock(fallBlock);

    // TODO put some speed throttling in there
    sf::Clock clock;
//    bool isPlaying = true;
    bool returnToTop = true,
         bottomContact = false,
         wentDown = false,
         sliding = false;

    // used for collisions to determine if a block can go to a new position
    unsigned posX, posY, oldPosX, oldPosY;
    
    while (window.isOpen())
    {
        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // releasing the escape key will close the app
            if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Escape)
                window.close();

            if (returnToTop) {
                posY = 0;
                posX = gridWidth / 2;
                returnToTop = false;
            }
            oldPosX = posX;
            oldPosY = posY;

            // One shot keyboard input
            if (event.type == sf::Event::KeyPressed) {
                std::cerr << std::endl;
                switch (event.key.code) {
                    case sf::Keyboard::Up:
                        if (posY > 0) posY--;
                        break;
                    case sf::Keyboard::Down:
                        if (posY < gridHeight-1) posY++;
                        break;
                    case sf::Keyboard::Left:
                        if (posX > 0) posX--;
                        break;
                    case sf::Keyboard::Right:
                        if (posX < gridWidth-1) posX++;
                        break;
                }

                std::cerr << "X: " << posX  << " Y: " << posY << " ";
                
                // cannot occupy existing block space
                if (gameGrid[posY][posX]) {
                    posY = oldPosY;
                    posX = oldPosX;
                    std::cerr << "touched ";
                }
                
                if ((posY == (gridHeight - 1)) || gameGrid[posY+1][posX]) {
                    // bottom making contact with floor or lower block
                    if (!sliding) {   // allow a slide before becoming static
                        sliding = true;
                        std::cerr << "slide == true ";
                    }
                    else {  // just became fixed
                        gameGrid[posY][posX] = 1;
                        returnToTop = true;
                        sliding = false;
                        std::cerr << "became static ";

                    }
                }
                else {    // slid off to avoid becoming fixed
                    std::cerr << "slid off ";
                    sliding = false;
                }
            }
        }

        // clear the window
        window.clear(sf::Color(sf::Color::Blue));

        // draw existing fixed blocks
        sf::Color gridColor;
        for (unsigned y = 0; y < gridHeight; y++) {
            // assume the row will be cleared unless...
            bool clearRow = true;

            for (unsigned x = 0; x < gridWidth; x++) {
                // green is landscape, black is the void
                gridColor = (gameGrid[y][x] ?  sf::Color::Green : sf::Color::Black);
                fixedBlock.setFillColor(gridColor);
                
                // scale the block to the desired size and draw it 
                fixedBlock.setPosition(x * blockSize.x, y * blockSize.y);
                window.draw(fixedBlock);
                
                // ...unless we hit an empty part of the row
                clearRow &= gameGrid[y][x];
            }

            // TODO start from bottom row
            // clear complete rows
            if (clearRow)
                for (unsigned x = 0; x < gridWidth; x++) gameGrid[y][x] = 0;
            // TODO shift rows down to replace vanished row
            // TODO fill gaps with blocks above if option is on
            // clear rows

        }

        // Draw the falling block
        fallBlock.setPosition(posX * blockSize.x, posY * blockSize.y);
        window.draw(fallBlock);

        // Update the window
        window.display();
    }
    
    return EXIT_SUCCESS;
}
